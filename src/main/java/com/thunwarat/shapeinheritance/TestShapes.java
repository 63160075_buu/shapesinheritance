/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.shapeinheritance;

/**
 *
 * @author ACER
 */
public class TestShapes {
    public static void main(String[] args){
        Shape shape = new Shape();
        //System.out.println(shape);
        shape.print();
        
        Circle circleA = new Circle(5);
        circleA.print();
        
        Circle circleB = new Circle(6);
        circleB.print();
        
        Rectangle rectangleA = new Rectangle(5,6);
        rectangleA.print();
        
        Squarl squarlA = new Squarl(3);
        squarlA.print();
        
        Triangle triangleA = new Triangle(5,6);
        triangleA.print();
        
        Shape[] shapes = {circleA,circleB,rectangleA,squarlA,triangleA};
            for(int i = 0;i<shapes.length; i++ ){
               shapes[i].print();     
      }
   }
}
