/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.shapeinheritance;

/**
 *
 * @author ACER
 */
public class Triangle extends Shape{
    private double x, y;
    
    public Triangle(double x, double y){
        System.out.println("Hi I am Triangle!!");
        this.x=x;
        this.y=y;
    }
    @Override
     public double calArea(){
        return (x*y)*0.5;
    }
    @Override
      public void print(){
        System.out.println("Triangle Of base Is " + this.x + " height = " + this.y + " Area = " + calArea());
    }
}
