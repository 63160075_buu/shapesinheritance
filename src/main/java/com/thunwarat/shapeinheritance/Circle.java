/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.shapeinheritance;

/**
 *
 * @author ACER
 */
public class Circle extends Shape {
    private double r;
    public static double pir = (22.0/7);
    
    public Circle(double r){
        System.out.println("Hi I am Circle!!");
        this.r=r;
    }
    @Override
     public double calArea(){
        return (r*r)*pir;
    }
    @Override
      public void print(){
        System.out.println("Circle Of r Is " + this.r + " Area = " + calArea());
    }
}
