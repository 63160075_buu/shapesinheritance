/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.shapeinheritance;

/**
 *
 * @author ACER
 */
public class Rectangle extends Shape{
    protected double wid, hi;
    
    public Rectangle(double wid, double hi){
        System.out.println("Hi I am Rectangle!!");
        this.wid=wid;
        this.hi=hi;
    }
    @Override
     public double calArea(){
        return wid*hi;
    }
    @Override
     public void print(){
        System.out.println("Rectangle Of width Is " + this.wid + " height = " + this.hi + " Area = " + calArea());
    }
}
